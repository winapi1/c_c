## Assumed Knowledge  미리 알아두면 좋은 지식

이 책은 운영체제 개발 초보자를 위해 쓰여졌지만, 프로그래밍에 대한 몇 가지 사전 경험을 권장합니다.
C를 가르치거나 컴파일러나 링커를 사용하는 방법을 알려주기 위한 것이 아닙니다.

코드에 버그가 있을 수 있고 독립 코드는 디버깅하기가 어려울 수 있습니다.
일부 하드웨어에는 직렬 포트가 포함되어 있지 않거나 실제 CPU에 하드웨어에 버그가 있을 수 있거나 사용자가 알지 못하는 아키텍처상의 특이점이 있습니다.

*직렬 포트 : 컴퓨터 환경에서 직렬 포트 또는 시리얼 케이블은 한 번에 하나의 비트 단위로 정보를 주고 받을 수 있는 직렬 통신의 물리 인터페이스이다.
병렬 포트와는 대조된다.
개인용 컴퓨터 역사를 보면 대개가 데이터는 단말기와 다양한 주변 기기와 같은 장치와 컴퓨터 사이에서 직렬 포트를 통해 전송된다. 

- C프로그래밍 언어의 중급 이해 숙달이 필요한 것은 아니지만 언어의 안과 밖, 특히 포인터와 포인터 산술에 대해 매우 잘 알고 있어야 합니다.
- 사용자 공간에서 코드를 쉽게 컴파일하고 디버깅할 수 있어야 합니다.
여러 에뮬레이터가 커널을 통과하는 데 사용할 수 있는 GDB 서버를 제공하므로 GDB가 권장됩니다.
- 침입적 링크된 목록과 같은 일반적인 데이터 구조를 사용하는 지식과 경헙입니다.
우리는 몇 가지 지점에서 배열 표기법을 사용하여 무슨 일이 일어나고 있는지 시작과 하는데 도움이 될 수 있지만, 고정된 크기 배열을 사용하여 커널에 임의의 제한을 두고 싶지는 않을 것입니다.

만약 여러분이 위의 내용에 대해 자신감을 느낀다면, 계속 읽어보세요! 
그렇지 않다면, 낙담하지 마세요.
학습할 수 있는 많은 자료들이 있고, 여러분은 언제든지 나중에 돌아올 수 있습니다.