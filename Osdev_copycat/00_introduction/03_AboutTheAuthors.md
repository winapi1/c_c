## 저자에 관한 정보

*너드 그리고 자유로운 프로그래머!! - 그냥 소프트웨어 개발을 좋아하고 컴퓨터에 관한 것은 레벨이 낮을수록 좋습니다.(기계어 쪽이 좋다는 이야기인듯)
저의 주된 작업은 소프트웨어 개발자일 뿐만 아니라 저의 주된 취미도 소프트웨어 개발자 입니다.
프로그래밍을 발견했을 때부터 저는 저만의 커널을 쓰고싶었습니다.
프로그래밍을 이해하는데 몇 년이 걸렸음에도 불구하고 말입니다.
커널을 쓰는 첫 번째 시도는 드림OS(32비트 커널)이었습니다.
자바, 파이썬, 고, 어셈블리어도 사용했지만 저의 주된 프로그래밍 언어는 C입니다. * - Ivan

*저는 취미를 가진 프로그래머이며 2021년부터 노스포트라고 불리는 운영체제 커널을 개발하고 있습니다.
저는 그 당시 몇 가지 다른 프로젝트, 즉 마이크로 커널과 윈도우 매니저를 실험했습니다.
OSDEV에 들어가기 전 저의 프로그래밍 관심사는 게임 엔진과 시스템 유틸리티였습니다.
제가 처음으로 완료한 프로그래밍 프로잭트는 C#의 태스크 매니저 클론이었습니다.
요즘 C++는 제가 선택한 언어이고, 저는 그 언어가 제공하는 자유가 트리플 오류를 일으킬 수 있는 자유로움을 좋아합니다. * - Dean.


## 우리의 프로젝트
* DreamOs64: A x86_64 kernel written in C, with memory management, scheduling and a VFS. Written by Ivan. [https://github.com/dreamos82/Dreamos64](https://github.com/dreamos82/Dreamos64)
* DRVOs: A tiny kernel, for riscv64, with very limited functionalities  by Ivan.  [https://codeberg.org/dreamos82/DRvOs](https://codeberg.org/dreamos82/DRvOs)
* Northport: 64-bit kernel supporting multiple architectures (x86_64 and riscv64) and SMP. Written in C++ by Dean. [https://github.com/DeanoBurrito/northport](https://github.com/DeanoBurrito/northport)
* Smoldtb: Tiny and portable device tree parser, written in C. by Dean. [https://github.com/DeanoBurrito/smoldtb/](https://github.com/DeanoBurrito/smoldtb/)