# 부트 프로토콜 s

부트 프로토콜은 커널이 부트로더에 의해 제어될 때 머신 상태를 정의합니다. 또한 머신의 메모리 맵, 프레임 버퍼, 때로는 uart 또는 커널 디버그 심볼과 같은 다른 유틸리티와 같은 여ㅑ러 서비스를 커널에 제공합니다.

이 장에서는 두 가지 프로토콜_멀티부트2_및_Limine 프로토콜을 다룹니다:

*_멀티부트2 는 grub의 기본 프로토콜인 멀티부트1을 능가합니다. 즉, grub이 설치된 곳이라면 어디든 멀티부트 커널을 로드할 수 있습니다. 대부분의 리눅스 컴퓨터에서 테스트가 쉽습니다. _멀티부트2_는 상당히 오래되었지만 매우 견고한 프로토콜입니다.

*_Limine Protocol_(Stivale 1과 2가 이전)인 Limine 부트로더의 기본 프토토콜입니다. Limine과 스티베일 프로토콜은 멀티부트 2 이후 수년이 지난 지금, 취미용 OS 개발을 더 쉽게 하기 위해 설계되었습니다. _Limine 프로토콜_을 읽기엔 더 복잡하지만 커널에 전달되기 전에 기계를 더 잘 알려진 상태로 둡니다(?)

Limine 프로토콜은 주로 아키텍처 변경 사항이 있지만 그 뒤에는 유사한 개념이 포함된 Stivale(이 책의 버전에서 다룸)를 기반으로 합니다. Stivale2의 개념을 잘 알고 있다면 Limine 프로토콜은 충분히 이해하기 쉽습니다.

참조된 모든 사양 및 문서는 이 장의 시작 부분에 링크로 제공됩니다.

## 이전 버전들은 어떠한가?

당연한 질문입니다. qemu/bochs/vmware/vbox 에서 테스트하는 세계에서는 UEFI나 BIOS를 직접 상대로 부트로더를 작성하는 것이 쉽습니다. 하지만 실제 하드웨어에서는 상황이 더 복잡해집니다.

제조업체가 사양을 정확히 따르고 모든 것이 설명된 대로 작동하는 CPU와 달리, PC 제조업체는 일반적으로 대부분의 사양을 따르며, 모든 기계에는 사소한 주의사항이 있습니다. 어떤 가정은 모든 곳에서 가정할 수 없으며, 어떤 기계는 사양을 완전히 깨뜨리기도 합니다. 이로 인해 어떤 기계에서는 엣지 케이스가 몇 개 있고, 어떤 기계에서는 모든 것이 엉망이 됩니다.

여기서 부트로더가 등장합니다: 커널과 PC 하드웨어의 혼란 사이에 추상화 계층이 있습니다. 부트 프로토콜을 제공하고(종종 우리가 선택할 수 있는 많은 것들 중에서), 그 프로토콜이 작동할 수 있도록 하드웨어 세계의 모든 것이 설정되도록 보장합니다. 이는 컨얼이 하드웨어 자체를 완전히 제어할 수 있는 충분한 드라이버를 설정할 때까지 입니다.

*저자 : 좋은 부트로더를 작성하는 것은 OSDEV 세계에서 고급 구제입니다. 새로운 경우 기존 부트로더를 사용하세요. 재미있는 프로젝트이지만 OS와 동시에 사용할 수는 없습니다. 기존 부트로더를 사용하면 앞으로 만은 문제를 줄일 수 있습니다. 또한 긴 모드로 전환하기 위한 어셈블리 스텁은 부트로더가 아닙니다.*

## 멀티부트 2

이 섹션에서는 주로 grub2 에 대해서 이야기 하겠습니다. grub 레거시라고 불리는 이전 버전의 grub가 있으며, grub 레거시를 *반드시* 실행해야 하는 하드웨어가 있다면 대부분의 버전 2 기능을 추가하는 레거시 패치가 있습니다. 이는 강력히 권장됩니다.

이러한 기능 중 하나는 grub가 64비트 elf 커널을 로드할 수 있는 기능입니다. 이를 통해 이전에는 32비트 elf와 64비트 커널을 모듈로 로드한 다음 64비트 elf를 수동으로 로드해야 했기 때문에 멀티부트 2를 사용하는 64비트 OS를 만드는 것이 크게 간단해집니다. 부트로더의 3단계를 효과적으로 다시 작성할 수 있습니다.

로드된 elf의 종류에 관계없이 멀티부트2는 잘 정의되어 있으며, 사양에 설명된 대로 CPU가 32비트 보호모드로 전환됩니다. 64비트 커널을 작성한다는 것은 설정하고 긴 모드로 진입하기 위해 수작업으로 만든 32비트 어셈블리 스텁이 필요하다는 것을 의미합니다.

두 프로톨의 주요 차이점 중 하나는 커널과 부트로더 간에 정보가 전달되는 방식입니다:
- _멀티부트1_은 커널 내에 고정된 크기의 헤더를 가지고 있으며, 이 헤더는 부트로더에 의해 읽힙니다. 이는 사용 가능한 옵션의 수를 제한하고, 모든 옵션이 사용되지 않을 경우 공간을 낭비합니다.
- _멀티부트2_는 헤더의 바이트 수 + 다음 모든 요청을 포함하는 'size'필드를 포함하는 고정된 크기의 헤더를 사용합니다. 각 요청에는 'identifier'필드와 일부 요청 특정 필드가 포함됩니다. 이는 약간 더 많은 오버헤드를 가지지만 더 유연합니다. 요청은 특별한 'null request'으로 종료됩니다.(이에 대한 사양 참조).

- _멀티부트 1_은 하나의 큰 구조를 통해 커널에 정보를 반환하며, 구조의 어느 부분이 유효한 것으로 간주되는지를 나타내는 비트맵을 포함합니다.
- _멀티부트 2_는 일련의 태그에 포인터를 반환합니다. 각 태그에는 내용을 확인하는 데 사용되는 '식별자' 필드와 다음 태그의 주소를 계산하는 데 사용할 수 있는 크기의 필드가 있습니다. 이 목록은 또한 특수 'null' 태그로 종료됩니다.

멀티부트 2에 대한 중요한 점 중 하나는 메모리 맵이 본질적으로 바이오스/UEFi에서 제공하는 맵이라는 점입니다. 부트로더 메모리(현재 gdt/idt와 같은), 커널 및 커널에 제공되는 정보 구조가 사용하는 영역은 모두 메모리의 *자유*영역에 할당됩니다. 명세서에는 메모리 맵을 커널에 제공하기 전에 이러한 영역을 *사용*으로 표시해야 한다고 명시되어 있지 않습니다. 이것이 실제로 grub가 이를 처리하는 방법이므로 메모리 맵에 대한 무결성 검사를 반드시 수행해야 합니다.   (성능때문에 명시를 안하는듯?)

### 부트 shim 만들기

멀티부트를 처음 시작할 때 가장 주의해야 할 점은  32비트 보호 모드로 전환된다는 점입니다. 이는 롱 모드를 수동으로 설정해야 한다는 것을 의미합니다. 또한, pmode에서는 페이징이 비활성화된 상태에서 실행되므로 번역이 불가능한기 때문에 커널을 상위 절만으로 매핑하기 위해 페이지 테이블 세트를 만들어야 한다는 것을 의미합니다.