# 1. Introduction

## 1.1 Executive Summary

## 1.2 Lazarus / Hidden Cobra - a North Korean APT group

## 1.3 Previous research

### 1.3.1 Lazarus'  attack against an Israeli  defense  company in 2019

### 1.3.2 Operation In(ter)ception: ESET's research on Lazarus' attacks published in June 2020

### 1.3.3 Operation North Star: McAfee's research on Lazarus' attacks published in July 2020

## 1.4 Attack Vector - Job Seekers' Recruitment Campaign

# 2. Tools Used by the Espionage Group in the "Dream Job" Campaign

## 2.1 Tools and Ofensive Techniques Categorized with MITRE ATT&CK

# 3. Social Engineering Attack Infrostructure

## 3.1 Introduction

## 3.2 Social Engineering  methods used to Establish a Credible Attack Infrastructure

### 3.2.1 First Stage - Creating a Reliable Fictitious Entity

### 3.2.2 Second Stage - Luring the victim via a Job Posting

### 3.2.3 Third Stage - Attacker-Victim Communication

# 4. Attack Scenarios and Tools Analysis

## 4.1 Introduction

## 4.2 First Stage - Infection

## 4.3 Second Stage - LNK and DLL Files

## 4.4 Third and Fourth Stages

## 4.5 Fufth Stages - Additional Tools


# 5. Attribution

## 5.1 Introduction

## 5.2 Code Overlap

### 5.2.1 Source Code Overlap

### 5.2.2 Similarities to known Lazarus Attack Tools and Advancement of Known Attack Methods New Attack Techniques

## 5.3 Techniques, Tatics and Procedures

## 5.4 Target Sectors

# 6. Summary and Insights

## 6.1 About the Attacker

## 6.2 Social engineering

# 7. Recommandations

## 7.1 Social Engineering

## 7.2 Attack Mitigation

# 8. Indicators of Compromise

## 8.1 Hashes

## 8.2 C2 Compromised Addresses