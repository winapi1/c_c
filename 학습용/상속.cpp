/*
상속 관련 내용
나무
살구 , 자두
플럼코트

나무안에 살구 자두가 있고, 플럼코트는 살구와 자두의 합작품이다.
*/

#include <iostream>

class Tree{
        int height; //명시를 안하면 기본으로 private
        int width;
    public:
        int leaves;

        Tree(int height=0, int leaves=0, int width=0){
            this->height = height;
		    this->leaves = leaves;
		    this->width = width; //this 꼭 써줘야해 나중에 상속할 때에 문제가 될 수 있기에 습관이 필요
        }

        Tree &grow(int height=1, int leaves=1, int width=1){
            this->height += height;
		    this->leaves += leaves;
		    this->width += width;

            return *this; //객체를 반환하기 위해서 만들었습니다, 신기하게 
        }

};

class SalguTree : public Tree{ //나무 상속 퍼플릭으로 받았기에 모든 접근제한은 부모 그대로
        int fruits;
        int vitaminA;
    public:
        SalguTree(int fruits=0, int vitaminA=0){
            this->fruits = fruits;
		    this->vitaminA = vitaminA;
        }

        int squeeze_vitaminA(){
            return this->vitaminA;
        }
};

class JaduTree : public Tree{
        int fruits;
        int vitaminC;
    public:
        JaduTree(int fruits = 0, int vitaminC = 0){
            this->fruits = fruits;
		    this->vitaminC = vitaminC;
        }

        int squeeze_vitaminC(){
            return vitaminC;
        }
};

class PlumcotTree : public SalguTree, public JaduTree{ //다중 상속을 사용하는데, 실제로는 자주 쓰지는 않는, 다른 언어에서는 없는 경우까지 있음
        int id;
        int locationX, locationY;
    public:
        PlumcotTree(int id = 0, int locationX = 0, int locationY = 0){
            this->id = id;
		    this->locationX = locationX;
		    this->locationY = locationY;
        }

    void print_location(){
        std::cout<<"location : "<<this->locationX << ", "<< this->locationY << std::endl;
    }
};

int main(){
    Tree inp(1,1,1);

    std::cout << inp.leaves << std::endl;
    
	std::cout << inp.grow().leaves << std::endl;

	PlumcotTree insot(0,300,400);	
	std::cout << insot.squeeze_vitmainA() << std::endl;

    std::cout << insot.leaves() << std::endl;
    /*
    다 잘 나오는데 여기만 문제가 생길것임
    보면 플럼코트 트리에 부모는 자두랑 살구인데 이 두 나무의 부모는 나무이다,
    그런데 나무에 잎 변수가 양쪽으로 내려가면서 살구를 타고가야하나 자두를 타고가야하나 문제가 생긴다.
    그래서 값에 접근은 가능한데 어느쪽의 값을 선택해야하는지 모르는 모호한 상황에 빠지기에 오류가 생긴다.
    */

    return 0;
}