#include <iostream>
#include <string>

//스트링 이용해서 함수의 api를 이용해보잦

using namespace std; //항상 앞에 붙이기 귀찮자나

int main(){
    string a = "anystring";
    string b = a,c=string(a); //b는 a내용 c는 a값 가져와서, 다르게 봐야하는
    string d = string("hi");
    string e= string("hianystring",2); //앞에 두개만

    auto it = a.begin(); //a
    
    for(;it!=a.end(); it++) // a내용 훑으면서 's' 를 찾아
    {
        cout << *it << endl;
        if(*it=='s')
            break;
    }
    string f = string(a.begin(),it); //it int값 
    cout << d << endl;
    cout << f << endl;

    cout << a+b+d << endl;

    size_t found = a.find("str");
    cout << "str found at : " << found << endl;
    // string 문서를 읽으면서 잘 이해하는 방법 * 나중에 API를 이용할 것이기에 꼭 필요한



    int inta = 20;
    double db = 3.0;
    string sc = to_string(db); //숫자를 스트링으로 슥

    cout << "int ro string : " << to_string(inta) << endl;
    cout << "string to float : " << stoi(sc)*2 << endl; //인트로 바꾸는 stoi


    string ts = "hi";

    ts[0] = 'Y';
    cout << ts << endl; //<<?



}