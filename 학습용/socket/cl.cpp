#include <stdio.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>


int main(){
    int clientfd;

    struct sockaddr_in server_addr;
    char *data = "hello";
    char buffer[1024] = {0};

    clientfd = socket(AF_INET, SOCK_STREAM, 0);

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(8090); 

    inet_pron(AF_INET, "127.0.0.1", &server_addr.sin_addr); //서버쪽 ip port넣어줘야지 암

    connect(clientfd, (struct socket*)&server_addr, sizeof(server_addr));

    send(clientfd, data, 20, 0); //헬로를 보낸다


    return 0;
}