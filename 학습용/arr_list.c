#include <stdio.h>
#include <stlib.h>
#include <string.h>


int g_count = 0; //노드 카운트용

void add_entity(struct node *); //컴파일을 위해 함수를 미리 작성
void delete_entity(struct node *);
void modify_entity*(struct node *);
void print_list(struct node *);

struct node{
    int id; //삭제 수정시 필요한 내용이기에 각 노드의 아이디가 필요할것
    char name[100];
    char phone[100]; 
}; //간단 노드 설정 ~ 전화번호부 내용이기에

int main(int argc, char **argc){// 기본이 int 리턴이며 인수 값도 프로그램이 실행될 때에 기본으로 부여되어야 한다.
    struct node list[100];
    int selection; // 노드 수랑 각 선택 가능한 배열수 0부터 시작하며 최대 100까지

    while(1){
        printf("1. add\n");
        printf("2. delete\n");
        printf("3. modify\n");
        printf("4. print\n");
        printf("sel >");

        scanf("%d", &selection);

        //1~4까지 숫자로 선택해서 삽입, 삭제, 수정, 전부 출력을 넣는다.
        //포인터로 넣지 않아도 배열로 보내기에 list그냥 넣으면 문제없다
        switch(selection){
            case 1:add_entity(list);
            break;
            case 2:delete_entity(list);
            break;
            case 3:modify_entity(list);
            break;
            case 4:print_list(list);
            break;
            default:break;
            //스위치에 브레이크는 항상 필수
        }
    }
    //매뉴가 계속 나오는 콘솔형이여야함, 간단한 매뉴 선택으로 수정, 삭제가 가능해야함


    return 0; // 240323 했나 기억이 안나네 가서 추가 필요
}

void add_entity(struct node *arr){ // 간단하게 노드 하나를 추가합시다.
    char name[100];
    char phone[100];

    printf("name : ");
    scanf("%s", name);

    printf("phone : ");
    scanf("%s", phone);

    arr[g_count].id = g_count; // ? 질문이 필요한??
    strcpy(arr[g_count].name,name);
    strcpy(arr[g_count].phone,phone);
    g_count++;//내용 추가 후 , 글로벌 카운트 업!
}



void delete_entity(struct node *arr){ //특정 노드 삭제
    int sel;
    printf("which entity :");
    scanf("%d", &sel); //한글자 한글자 치면 인터럽트가 발생해서 잡을 발생 하나씩 넣어두고 엔터를 치면 워커에 들어가서 어떤 프로세스에서 이런 데이터가 나왔다 저장, 이후 프로세스를 돌다가 필요하면 가져와서 사용한다.
    
    for(int i = sel; i < g_count-1; i++){
        memcpy(arr+i, arr+i+1, sizeof(struct node)); //선택한 노드의 내용을 다음 위치의 노드 내용을 가져와 덮어쓰는 방식으로 삭제한다
    }
    g_count--; // 노드 하나가 사라졌으니 필요
    memset(arr+g_count,0,sizeof(struct node)); //추가적으로 필요 마지막 노드의 위치를 초기화 하던거인가? 물어볼 필요가 있을듯++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
}
//最近「ソ·ジェロ」という海外旅行ユーチューバーを見るのに本当に良い。


void modify_entity(struct node *arr){
    int sel;
    char name[100];
    char phone[100]; //수정 받을 숫자랑 내용들 ~ strcpy 사용이 중요
    
    printf("which entity? : ");
    scanf("%d", &sel);

    print("name : ");
    scanf("%s", name);

    printf("phone : ");
    scanf("%s", phone);

    strcpy(arr[sel].name, name);
    strcpy(arr[sel].phone, phone); //왜 strcpy가 더 좋은가?
}


void print_list(struct node *arr){
    system("clear"); //리눅스의 그 클리어 기능 깨끗하게 보기가 좋다
    printf("\n\n\n\n===============================================\n");
    for(int i = 0; i < g_count; i++)
        printf("%d. %s = %s", arr[i].id, arr[i].name, arr[i].phone); //아이디, 이름, 전번
    printf("===============================================\n");

}