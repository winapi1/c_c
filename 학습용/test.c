#include <stsio.h>

void dictionary(){ //배열을 포인터로 연결해서 프린트
    char a[10] = "apple";
    char b[10] = "banana";
    char c[10] = "cat";

    char *diction[3];
    diction[0] = a;
    diction[1] = b;
    diction[2] = c;

    for(int i = 0; i < 3; i++){
        printf("%s\n", diction[i]);
    }
}

void swap(int *a, int *b){ //값 바꾸기 ~ 실제 값을 바꾸기 위해서 포인터로
    int temp;
    temp = *a;
    *a = *b;
    *b = temp;
}

int max(int *ar){ //최대값 찾기
    int i = 0;
    int max = -1; //보통 구현 가능한 가장 작은 수를 넣지만 양수만을 사용하기에 음수를 삽입

    for(int i = 0; i <= 10; i++){ //배열 크기를 따로 받거나 해야하는데 편의상 10개의 배열이라 생각하고 사용
        if(max < ar[i])
            max = ar[i];
    }
    return max;
}

void gugudan(){ //구구단 1단부터 9단까지
    int x,y;
    
    for(x = 1; x <= 9; x++){
        for(y = 1; y <= 9; y++){
            printf("%d x %d = %d   ", x, y, x*y);  
        }
        println("");
    }
}
//パソコンの仕組みを知っておくといいですね
//os 관련 내용 한글로 다시쓰면 좋을듯

int main(){
    int a = -10;
    int b = 10;
    println("a = %d, b = %d", a,b);
    swap(&a,&b);
    println("a = %d, b = %d", a,b); //swap요청을 위해 포인터를 통한 실제 값을 변경했기에 제대로 값이 바뀐다, 포인터를 사용하지 않는 함수를 이용한다면 이렇게는 나오지 않을것





    return 0;
}
