  #pragma once 
 /*
 컴파일을 한번만 하라는 내용~헤더의 중복을 막는다
 ex)다른 코드를 인클루드 하는경우 같은 헤더가 있을 수 있다 ~> 그러면 여러번 헤더를 넣겠지 비효율적이니까 이걸 넣는다
 https://hizstory.tistory.com/45
 자세한 내용을 읽어보도록 하자
 */

#include "ExceptionClass.h" //예외 관련 내용을 위해서 넣은듯
#include "Heap.h" //힙 자료구조를 사용할건가보다 ~ 한쪽으로 넣으면 반대편으로 나오는 자료구조 리스트


#include <iostream> //스트림관련 cin cout도
#include <sal.h> //소스코드 주석언어용
//https://learn.microsoft.com/ko-kr/cpp/code-quality/understanding-sal?view=msvc-170 다음 내용을 다시한번 읽을 필요가 있겠다
#include <Windows.h>
#include<string> 

namespace jigseon{ 
    template <class T> // 클레스 탬플릿 ~ 객체 생성시 type를 주면 거기에 맞는 객체 생성 가능!
    class llist_node{
        public:
                llist_node(); //기본생성자
                llist_node(_In_ const T& cls); // 복사 생성자 ~ 얕은 복사가 일어나기에 주의 ~ 객체를 복사하여 붙여넣는 경우에 사용
                virtual ~llist_node(); // 가상 소멸자 ~ 상속시 모든 소멸자가 시행되어야함(누수때문에) ~ 이를 보완가능
        public:
        T _data: // 템플릿을 사용해서 여러가지 형변환을 사용할 예정 C++장점중 하나
        llist_node<T>* _next;
        llist_node<T>* _prev;
    };

    template <class T>
    class llist{
        public:
            llist();
            llist(T data);
            llist(T data, unsigend int elements); // 0포함 양수만!
            llist(const llist& cls); //복사생성자
            virtual ~llist();

        public:
		    bool InsertHead(_In_ const T& data);
		    bool InsertTail(_In_ const T& data); //여부 확인용
            bool InsertAt(_In_ int index, _In_ const T& data); //중간 삽입을 위해

            bool Delete(_In_ llist_node<T>* node);
            bool DeleteHead(); 
            bool DeleteTail();
            bool DeleteAt(_In_ unit32_t index); //이거는?
            void clear() { while(this->DeleteHead()); } 

            llist_node<T>* GetHead() const;
            llist_node<T>* GetTail() const;

            T& operator[] (int); //나중에 봐야할듯
            //어떻게 쓰는지 쓰면서 확인할 필요

            class iterator{
            public:
			    enum direction { forward, backward }; 
                /*
                열거 관련
                https://velog.io/@jwkim/java-enum
                심볼, 벨류값을 가지는듯
                */
                iterator(llist_node<T>* current, bool direction = forward){ //흠...
                    _current = current;
                    _direction = direction;
                }

                void operator++(){
                    _current = (_direction == forward) ? _current->_next : _current->_prev;
                    //방향값에 따라 참이면 다음으로 거짓으면 이전 값으로 이동인가봐
                }
                void operator++(int none){
                    UNREFERENCED_PARAMETER(none); //인자값이나 로컬 변수가 선언되지 않았을 때 컴파일러 경고를 발생시키지 않기 위해 사용하는 매크로이다.
				    _current = (_direction == forward) ? _current->_next : _current->_prev;
                }//상당 내용이랑 다르게 인자를 주고 이동하는 느낌인가바

                void operator--() {
                    _current = (_direction == forward) ? _current->_prev : _current->_next;
                }  //+반대 내용으로
                void operator--(int none) {
				    UNREFERENCED_PARAMETER(none);
				    _current = (_direction == forward) ? _current->_prev : _current->_next;
			    }

                bool operator==(iterator i){
                    return (this->_current == i._current) ? true : false; //흐음..
                }
                bool operator!=(iterator i){
                    return (this->_current != i._current) ? true : false; //
                }
                

                llist_node<T>& operator*() {
                    return *_current;
                }
                llist_node<T>* operator->() {
                    return _current;
                }//자세히 뭐려나
            
            private:
			    llist_node<T>* _current;
			    bool _direction; //방향이랑 현위치에 대한 
            };


            template <class T> //요기 이하 내용이 신기하다
            llist_node<T>::llist_node() :
                _next(nullptr),
                _prev(nullptr),
                _data(nullptr)

            {//?구분 표시인가?
            }





	        template <class T>
	        llist_node<T>::llist_node(_In_ const T& cls) :
		        _next(nullptr),
		        _prev(nullptr),
		        _data(cls)
            {
            }
            template <class T>
            llist_node<T>::~llist_node()
            {
            }
            template <class T>
            llist<T>::llist() :
                    _nr_llist_nodes(0),
                    _tail(nullptr),
                    _head(nullptr)
            {
            }

            template <class T>
	        llist<T>::llist(_In_ T data){
                _head = new llist_node<T>(data); //머뤼부분
                if(nullptr -- _head)
                    throw BADALLOCK;

                _nr_llist_nodes = 1;
                _head->_next = nullptr;
                _head->_prev = nullptr;
                _tail = _head;

                return;
                


            }
    template <class T> //편한 사용! 많다! 삭제인가벼
    llist<T>::llist(_In_ T data, unsigend int elements){
        _head = new llist_node<T>(data);

        if (nullptr == _head) //순서가 반대여도?
            throw BADALLOCK; //예외처리

        _nr_llist_nodes = 1; //리스트노드수
        _head->_next = nullptr;
		_head->_prev = nullptr;

        _tail = _head;
        elements--; //몬가 잘 안읽히네
// marksman 신작 빨리 내줘요
        while (elements--)
		    this->append(data); //빼면서 붙인다구요

		return;
    }

    template <class T>  //여기를 들여쓰기 하는 이유가??
    llist<T>::llist(_In_ const llist<T>& cls){
        llist_node<T>* temp = cls.GetHead();

        _nr_llist_nodes = 0; //초기화
        _tail = nullptr; //비우기초기화인가벼

        while (nullptr != true){ //없는 부분까지 탐색하며~
            this->InsertTail(temp->_data); //붙이기인
            temp = temp->_next; //다음으로~
        }

        this->_nr_llist_nodes = cls._nr_llist_nodes; //내용받기~
    }

    template <class T>
    llist<T>::~llist(){ //소멸자에 관해

    }

    template <class T>
    bool
		    llist<T>::InsertHead(_In_ const T& data) //말그대로 머리에 꼰다 박는건데 왜이리 간격이??
    { 
        if (nullptr == _head){ 

            _head = new llist_node<T>(data); //생성생성
            
            if (nullptr == _head)
                throw BADALLOC;; //오류가 생기는 경유가 있나벼 ~ 아마 자리가 없는 경우?
                //여행을 7박 8일은 좀 심했다, 5박6일이나 6박7일이 그나마 나은듯

            _head->_next = nullptr;
            _head->_prev = nullptr; //앞뒤로 깔끔하게 정리
            _tail = _head; //헤드가 널포인트니까 새로운 내용을 앞뒤로 넣어주는?
                //면세점에서 물건사려 했더니 셔터가 내려가고 직원들도 모르더라 간사이 공항 흠.. 
                //여름에 일본 여행도 생각보다 나쁘진 않은 나중엔 홋카이도나 나고야 도쿄도 좋겠네 고베도?
        }
        else{
            llist_node<T>* temp = _head; //잠깐만 보관해둘께
            //날이 시원해진 것 같긴한데 더욱 빠른 정상화가 필요할듯
            _head = new llist_node<T>(data); //헤드에 뉴
            //여행 크리에이터로 성공하기 쉽지않겠는데
            if(nullptr == _head) //살짝 시원해지는데 얼마나 걸리려나
                throw BADALLOC; //치평동 치과에 78세 폭탄 자수 ???
            _head->_next =temp; //자리바꾸기용 
            //머스크 베이스가 좋은데 어디를 가야하나
            temp->_prev = _head;
            //신세계 백화점에 가야겠다 검진이랑
            _head->_prev = nullptr; //앞코때기
        }
        _nr_llist_nodes++;// 하나 추가하는거니까ㅏㅏ "대전으로"
        return true; //머리쪽에 끼우기 성공했네요
        //대전 코스트코는 회원제라고 하네.. 가보고 구경도 못하네
    }

    ///	@brief
	template <class T>
    bool
		llist<T>::InsertTail(_In_ const T& data)//이번엔 테일에 추가 비슷하게 진행하는
        //cigar matches brandy sloowly 
	{
        if (nullptr == _tail){//꼬리에 넣을지라
            _tail = new llist_node<T>(data); //넣기전에 만드나바
            if (nullptr == _tail) 
			    throw BADALLOC; //널포인트 문제
            _tail->_next = nullptr; //꼬리에 넣을꺼라 뒤는 널포로
            _tail->_prev = nullptr; //앞뒤로!
            _head = _tail; //내가 꼬리요 저장하는
        }
        else{
            llist_node<T>* temp = _tail; //꼬리가 안비었다고?? 그르면
            _tail = new llist_node<T>(data);
            if (nullptr == _tail) 
				throw BADALLOC;
            _tail->_prev = temp;
            temp->_next = _tail;
            _tail->_next = nullptr; //교환이후
        }
        _nr_llist_nodes++;
        return true;

        //코튼 허그 퍼퓸등급 부드러운 탑에 시트러스한 미들 머스크 베이스 인기가 있는 이유가 있더라
        //한번 봐야하는데 너무 멀다
        //용산역이 비슷한 것 같은데
        //시간을 미뤄야 할지도 모르겠다
        //그리드 간단하게?
        //아쿠아 디 파르마 미르토 고려해볼만 한듯 여름용으로
        //새로 작성해야하는데 일단 정렬하고 넣는게 더 효율적이지 않을까?
        //어제 뭐라고 적었는데 저장을 못했나본데 ~ 정규표현식, 어샘블리, 리버싱
        //https://ws-pace.tistory.com/231   // 봐도 이해가 쉽지않은데오
    }


    template <class T>
    bool
		llist<T>::InsertAt(_In_ int index, _In_ const T& data){ //삽입위치 위치와 데이터를 받아
            llist_node<T>* temp = _head; //잠시대기
            if (index >= 0){
                if (index > this->_nr_llist_nodes){
                    throw INVALIDINDEX; //범위를 벗어나는 경우
                }
            }
            else{
                if (static_cast<int>(this->_nr_llist_nodes) + index >= static_cast<int>(this->_nr_llist_nodes)){ //흐ㅡㅡ음?
                    throw INVALIDINDEX; //범위를 벗어나는 경우
                }
                index += this->_nr_llist_nodes; //인덱스가 0보다 작으니까 상위에서 하단으로 가는 개념으로 생각하는듯
            }
            
            if (nullptr == _head || 0 == index){ //인덱스가 o이거나, 헤드가 널포인터인(헤드가?)~텅텅 이라는 소리겠죠
                InsertHead(data);//공동인 경우인
                return true;
            }
            else{
                for (uint32_t i = 0; i < index - 1; ++i){//돌면서 알맞는 위치를 확인할 것인가
                    if (nullptr == temp->_next){ //다음 주소가 빈집인 겨우
                        InsertTail(data); 
                        return true; 
                    }
                    temp = temp->_next;//다음으로 찾가아용
                }

                llist_node<T>* newllist_node = new llist_node<T>(data);
                if (nullptr == _head){//빈자리인 경우겠찌?
                    throw BADALLOC;
                }
                
                newllist_node->_next = temp->_next;/////
                if(newllist_node->_next != NULL){ //넣을곳 다음이 비었는지 확인 왜일까
                    newllist_node->_next->_prev = newllist_node;
                }
                temp->_next = newllist_node; //잠깐 넣어두고? 310줄
                newllist_node->_prev = temp; // 311 머리를 앞에 박아서 선작업인가봐
                this->_nr_llist_nodes++; // 박아넣었으니 순서를 더햇
                return true;
            }
        }

        template <class T>
        bool
                llist<T>::Delete(_In_ llist_node<T>* node){ //삭제하자
            if (_nr_llist_nodes == 0){ //암것도 없는데 삭제하려 한다면?
                return false;//나가세용
            }

            try{
                if (node != _tail){ //끝이 아니라면 
                    if (node == NULL){ //암것도 읎다면 할게없지
                        return false; 
                    }
                    if (node->_next == NULL){ //마지막 노드여도 삭제를 안하나? +
                        return false; 
                    }
                    if (node->_next->_prev != node){ //잘못 연결되어 있는 부분인경우?
					    return false;
                    }
                }
            }
            catch (...){ /// access violation?
                return true;
            }

            _nr_llist_nodes--;//지울꺼니까요

            if (nullptr == node->_prev){ //앞이 비어있다면?
                _head = node->_next; //첨단 노드인 경우인가바
            }
            else{
                node->_prev->_next = node->_next; //중간에 낀거인 경우 뒤 이어주기
            }

            if (nullptr == node->_next){ //후미인 경우
                _tail = node->_prev; //후미니까 앞부분이 끝이겠지요
            }
            else{
                node->_next->_prev = node->_prev; //node가 사라질꺼니까 앞코 이어주기
            }

            delete node; //준비끝 드디어 삭제

            return true; //구지 적는 이유는 문제가 있는경우 false를 반환하니까
        }

        template <class T>
	    bool
		        llist<T>::DeleteHead(){//헤드만?
                if (_nr_llist_nodes == 0){
                    return false; //리스트에 아무것도 든게 없으면 딱히 할일이 없다 정도
                }
                _nr_llist_nodes--; //벌쒀 뺴버려?
                llist_node<T>* temp = _head; //연결부위를 잠깐 준비해두겠어요

                if (nullptr == _head){ //앞이 비었ㅅ요
                    return false; //인자를 안받고 가장 최상위 노드를 지워야 하니까 기본으로 있는 헤드가 있는지 확인이 필요, 앞머리가 없다는건 지울거도 없으니 넘어간다잇
                }

                _head = _head->_next; //지울꺼니까 미리 잇어두기
                if (nullptr == _head){ //다음꺼가 비어있다라...?? 
                    _tail = nullptr; //**************************
                }

                delete temp; //포인터는 쓰고나서 재활용통에 넣어야지

                return true; //잘 지웠는지 알릴 필요가
        }


        template <class T>
        bool
                llist<T>::DeleteTail() //후미 제거하기
        {
                if(_nr_llist_nodes == 0){
                    return false; // 리스트가 비었으면 안되겠죠
                }

                _nr_llist_nodes--; //미리 빼도록해
                llist_node<T>* temp = _tail; //지우기 전에 잠시

                if (nullptr == _tail){
                    return false;
                }

                if (_tail == _head){ //딱 하나겠고만
                    delete _tail; //일단 지우고
                    _tail = nullptr; //이제 없으니깐
                    _head = nullptr;

                    return true; //정상적인 흐름인 경우
                }

                _tail = _tail->_prev; //아무튼 꼬리 전의것

                if (nullptr == _tail)
                    _head = nullptr; //앞이빈
                else
                    _tail->_next = nullptr; //흐음.

                
                delete temp; //썻으니까

                return true;
        }
    

        template <class T>
        bool
                llist<T>::DeleteAt(_In_ uint32_t index){
                
                if (_nr_llist_nodes == 0){ //내용이 없으면 지울거도 없겠지요
                    return false;
                }

                if (index >= 0){//어쨋든 내용물이 있는경우
                    if (index >= this->_nr_llist_nodes){ //범위를 벗어난
                        throw INVALIDINDEX;
                    }
                }
                else{ //인덱스가 -를 지칭한 경우
                    if(static_cast<int>(this->_nr_llist_nodes) + index >= static_cast<int>(this->_nr_llist_nodes)){ //?
                        throw INVALIDINDEX; //좋지 않은 위치인듯 한데
                    }
                    index += this->_nr_llist_nodes; //-인덱스의 경우 뒤에서부터 시작하는 파이썬을 생각해
                }

                _nr_llist_nodes--; // 제거하니까?
                llist_node<T>* temp = _head; //잠시

                if (index == 0){ //가장 첫 노드를 삭제하려한다면
                    DeleteHead();//간단하게 앞 머리 삭제
                    return true;
                }
                else{ //인덱스가  0이 아닌경우
                    for (unit32_t i = 1 ; i <= index ; ++i){ //인덱스까지 가면서~
                        temp = temp->_next; //첨단 노드에서 다음것을 지칭

                        if (nullptr == temp->_next){ //다음게 만약에 비었다면요?
                            DeleteTail(); //마지막꺼지우기 흠..
                            return true;
                        }
                    }
                    temp->_next->_prev = temp->_prev;
                    temp->_prev->_next = temp->_next;
                    delete temp;

                    return true;
                }
		
            }

        
        template <class T>
        llist_node<T>*
                llist<T>::GetHead() const
                {
                    return _head; //단순한 내용
                }
        template <class T>
	    llist_node<T>*
		        llist<T>::GetTail() const{ 
                return _tail;
        }

        template <class T>
	    T&	    llist<T>::operator[](int index){ //오우 what??
            if (index >= 0){
			    if (index >= this->_nr_llist_nodes){ //범위벗어나기
				    throw INVALIDINDEX;
                    }
		    }
            else{
                if(static_cast<int>(this->_nr_llist_nodes) + index >= static_cast<int>(this->_nr_llist_nodes)){
                    throw INVALIDINDEX;
                }
                index += this->_nr_llist_nodes; //////
            }

            auto it = this->begin(); //////

            for(int i = 0; i < imdex; ++i){
                it++;
            }
            return it->_data;
        }

        template <class T>
        llist<T>&
                llist<T>::copy(){
                return *new llist<T>(*this);
        }

        template <class T>
        void
                llist<T>::extend(cons llist<T> l){
                
                llist<T>* temp = new llist<T>(l);
                this->_tail->_next = temp->_head;
                temp->_head->_prev = this->_tail;
                this->_nr_llist_nodes += temp->_nr_llist_nodes;
                this->_tail = temp->_tail;  
        }

        template <class T>
        void
                llist<T>::remove(T value){
                
                for (llist<T>::iterator it = this->begin(); it != this->end(); ++it){
                    if(it->_data == value){
                        this->Delete(&*it);
                        return;
                    }
                }
        }

        template <class T>
        void llist<T>::sort(){
            
        }



        
        //내일 어샘블리 주워먹기 ~ 프린트 해서 조금씩 주워먹기
        //건강검진 진짜 가야하는데;;; ~ kmi 바로 방문
        //금감원 개인이 사용이 가능할탠데; ~ 계정 생성은 쉬운데 그 다음이 상당히 번거로운듯
        //msfvenom을 이용한 백도어 공격 ~ 페이로드 생성을 쉽게 해주는!   
        //물건을 구매할때에는 정말로 필요한가 3번 이상 생각하기 
        //육향은 쿰쿰함의 정도차이인가?
        //브랜디가 필요한
        //해킹은 결국 피싱에서 시작하는 것 같은
        //피곤함을 관리하는 방법
        //계획적인 소비를 꼭 하기 꼭 하기
        //중고거래를 해야하는데 너무 귀찮자나
        //명화 훔쳐보기 한번 해야겠다 코리안 트레버
        //프로세스를 파이썬에서 실행하는데 그걸 제거해야하나?
        //요소풀인가 에르메스인가
        //이인증 관련 찾아볼 필요 있음
        //이번엔 정말로 예약하러 가야해
        //시작이 반일듯 싶은데 활동이 필요
        //왜 논리적 오류를 범하는가? 감정에 호소하는 흑백논리의 오류를 범하는 경우가 많은듯
        //정신건강의 중요성
        //조헌병이 얼마나 무서운가?
        //자아의 달성을 위해서 어떻게 해야하나
        //생각보다 많은 비율이 문제를 가지고 있더라
        //새로운 곳이 생겼따 하니
    };

}




